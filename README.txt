-- INSTALLATION --

* Install as usual Drupal module, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
This payment method can be configured by users with permission "Administer payment methods".
1. This payment method can be configured as other payment methods. After enabling the module, "PB Platba Online" should appear in the list of payment methods,
(it is page /admin/commerce/config/payment-methods)
2. After clicking on ""PB - Platba Online"" in the list, 'Editing reaction rule ""PB - Platba Online""'  page appears
(page admin/commerce/config/payment-methods/manage/commerce_payment_tatrapay)
Tatrapay settings (like Merchant ID, safe key) can be edited in "Actions" table


-- HOW IT WORKS

When "PB - Platba Online" payment method is enabled, it can be chosen by users as payment method during checkout process.
When customer choose "PB - Platba Online", user is redirected offsite to third-party ("PB - Platba Online") website, where the payment will be processed. 
After payment is processed, user is redirected from "PB - Platba Online" website back to eshop to continue (and finish) checkout process.
After return to eshop, this module automatically update status of payment transaction (payment transaction status is set to "success").

Admistrators of eshop can check payment transaction status of particular orders on "Payment" tab on order page (admin/commerce/orders/[ORDER ID]/payment)




